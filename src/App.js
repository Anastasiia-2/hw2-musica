import { useState, useEffect } from "react";
import "./App.css";
import ItemsContainer from "./components/ItemsContainer/ItemsContainer";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";

function App() {
  const [items, setItems] = useState([]);
  const [currentItem, setCurrentItem] = useState({});
  const [cartItems, setCartItems] = useState([]);
  const [isShowModal, setIsShowModal] = useState(false);

  const addToCartList = () => {
    const { src, price, name, id } = currentItem;

    setCartItems(prevState => {
      const newCartItemsState = [...prevState];
      const itemCart = newCartItemsState.find(item => item.id === id);
      if (itemCart) {
        itemCart.count++;
        localStorage.setItem("cart", JSON.stringify(newCartItemsState));
        return newCartItemsState;
      } else {
        localStorage.setItem(
          "cart",
          JSON.stringify([{ src, price, name, id, count: 1 }, ...prevState])
        );
        return [{ src, price, name, id, count: 1 }, ...prevState];
      }
    });
    toggleModal();
  };

  const toggleModal = () => {
    setIsShowModal(prev => !prev);
  };

  useEffect(() => {
    const lsFavItems = localStorage.getItem("favor");

    if (lsFavItems) {
      setItems(JSON.parse(lsFavItems));
    } else {
      (async () => {
        try {
          const data = await fetch("./itemsList.json").then(res => res.json());
          setItems(data);
        } catch (error) {
          console.log(error);
        }
      })();
    }

    const lsCartValue = localStorage.getItem("cart");
    if (lsCartValue) {
      setCartItems(JSON.parse(lsCartValue));
    } else {
      setCartItems([]);
    }
  }, []);

  return (
    <div className="App">
      <Header cartItems={cartItems} items={items} />
      <h2>Our products</h2>
      <ItemsContainer
        items={items}
        setCurrentItem={setCurrentItem}
        toggleModal={toggleModal}
        setItems={setItems}
      />
      {isShowModal && (
        <Modal
          header="Add to cart"
          closeButton={true}
          handleBtnClick={toggleModal}
          text="Do you want to add this product to your cart?"
          actions={
            <>
              <Button text="Yes" handleBtnClick={addToCartList} />
              <Button text="No" handleBtnClick={toggleModal} />
            </>
          }
        />
      )}
    </div>
  );
}

export default App;
