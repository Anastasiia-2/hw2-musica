import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styles from "./Header.module.scss";

import Logo from "../Logo/Logo";
import AdditionalInformaition from "../AdditionalInformaition/AdditionalInformaition";

export default function Header({ cartItems, items }) {
  const [cartAmount, setCartAmount] = useState(0);
  const [favAmount, setFavAmount] = useState(0);

  useEffect(() => {
    setCartAmount(cartItems.reduce((acc, item) => (acc += item.count), 0));
  }, [cartItems]);

  useEffect(
    () =>
      setFavAmount(
        items.reduce((acc, item) => {
          if (item.isFavorited) {
            return (acc += 1);
          }
          return acc;
        }, 0)
      ),
    [items]
  );

  return (
    <header className={styles.headerContainer}>
      <Logo />
      <AdditionalInformaition cartCount={cartAmount} favСount={favAmount} />
    </header>
  );
}

Header.propTypes = {
  cartItems: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
};
