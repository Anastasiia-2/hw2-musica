import React from "react";
import PropTypes from "prop-types";
import { AiOutlineHeart, AiFillHeart } from "react-icons/ai";
import { BsCartPlus } from "react-icons/bs";

import styles from "./Item.module.scss";
import Button from "../Button/Button";

export default function Item(props) {
  const { item, toggleModal, setCurrentItem, setItems } = props;

  const { src, price, name } = item;

  const handleAddToCartClick = () => {
    setCurrentItem(item);
    toggleModal();
  };

  const handleFavBtnClick = e => {
    setItems(prev => {
      const newItems = [...prev];
      const index = newItems.indexOf(item);
      newItems[index].isFavorited = !item.isFavorited;

      const newItem = newItems[index];
      newItems.splice(index, 1, newItem);
      localStorage.setItem("favor", JSON.stringify([...newItems]));

      return [...newItems];
    });
  };

  return (
    <li className={styles.item}>
      <div className={styles.itemImg}>
        <img src={src} alt={name} />
        <Button
          text={<BsCartPlus size="24" />}
          handleBtnClick={handleAddToCartClick}
          classBtn={styles.itemAddToCartBtn}
        />
        <Button
          text={
            item.isFavorited ? (
              <AiFillHeart size="24" className={styles.icon} />
            ) : (
              <AiOutlineHeart size="24" className={styles.icon} />
            )
          }
          handleBtnClick={handleFavBtnClick}
          classBtn={item.isFavorited ? styles.favorite : styles.itemAddToFavBtn}
        />
      </div>
      <div className={styles.textWrapper}>
        <p className={styles.itemTitle}>{name}</p>
        <p className={styles.itemPrice}> {price}</p>
      </div>
    </li>
  );
}

Item.propTypes = {
  item: PropTypes.object.isRequired,
  toggleModal: PropTypes.func.isRequired,
  setCurrentItem: PropTypes.func.isRequired,
  setItems: PropTypes.func.isRequired,
};
