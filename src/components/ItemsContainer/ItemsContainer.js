import React from "react";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";

export default function ItemsContainer({
  items,
  setCurrentItem,
  toggleModal,
  setItems,
}) {
  return (
    <ul className={styles.list}>
      {items &&
        items.map(item => {
          return (
            <Item
              key={item.id}
              item={item}
              toggleModal={toggleModal}
              setCurrentItem={setCurrentItem}
              setItems={setItems}
            />
          );
        })}
    </ul>
  );
}
